%bcond_with tests

Name:          tpm2-tss
Version:       4.0.2
Release:       2
Summary:       TPM2.0 Software Stack
License:       BSD-2-Clause
URL:           https://github.com/tpm2-software/tpm2-tss
Source0:       https://github.com/tpm2-software/tpm2-tss/releases/download/%{version}/%{name}-%{version}.tar.gz

Patch0001:     backport-FAPI-Skip-test-fapi-fix-provisioning-with-template-i.patch
Patch0002:     backport-esys-add-SM4-algorithm-support.patch
Patch0003:     Hygon-Add-support-for-udev-to-create-tcm-de.patch
Patch0004:     Hygon-add-ecc-encrypt-decrypt-support.patch
Patch0005:     add-bootstrap-file-from-upstream.patch

BuildRequires: gcc-c++ make libtool
BuildRequires: pkgconfig(cmocka) >= 1.0
BuildRequires: pkgconfig(json-c)
BuildRequires: pkgconfig(libcrypto) >= 1.1.0
BuildRequires: pkgconfig(libcurl)
BuildRequires: pkgconfig(uuid)
BuildRequires: libtpms-devel
BuildRequires: uthash-devel
BuildRequires: /usr/bin/swtpm
BuildRequires: /usr/sbin/ss
BuildRequires: doxygen

%description
tpm2-tss is a software stack supporting Trusted Platform Module(TPM) 2.0 system
APIs which provides TPM2.0 specified APIs for applications to access TPM module through kernel TPM drivers.

%package devel
Summary: Headers and libraries for building apps that use tpm2-tss
Requires: %{name} = %{version}-%{release}
Obsoletes: %{name}-static < %{version}-%{release}

%description devel
It contains headers and static libraries for tpm2-tss.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
./bootstrap
%configure --disable-static --disable-silent-rules --with-udevrulesdir=%{_udevrulesdir} --with-udevrulesprefix=80- \
           --with-runstatedir=%{_rundir} --with-tmpfilesdir=%{_tmpfilesdir} --with-sysusersdir=%{_sysusersdir} \
	   %{?with_tests:--enable-unit --enable-integration}

%make_build

%install
%make_install
%delete_la

%check
%make_build check

%files
%doc README.md CHANGELOG.md
%license LICENSE
%{_sysconfdir}/tpm2-tss/
%{_sysusersdir}/tpm2-tss.conf
%{_tmpfilesdir}/tpm2-tss-fapi.conf
%{_libdir}/*.so.*
%{_udevrulesdir}/80-tpm-udev.rules

%files devel
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files help
%{_mandir}/man*/*

%changelog
* Fri Aug 2 2024 chench <chench@hygon.cn> - 4.0.2-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add SM4 algorithm support
  - Add support for udev to create tcm devices
  - add ecc encrypt/decrypt support
  - add bootstrap file from upstream

* Thu Oct 03 2024 Funda Wang <fundawang@yeah.net> - 4.0.2-1
- update to 4.0.2
- disable test for now due to it produces linking errors when enabling LTO

* Wed May 8 2024 jinlun <jinlun@huawei.com> - 4.0.1-2
- fix CVE-2024-29040 and fix test check error

* Tue Jan 23 2024 jinlun <jinlun@huawei.com> - 4.0.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 4.0.1
  - Fix CVE-2023-22745
  - TPM version 1.59 support
  - libmu soname from 0:0:0 to 0:1:0.
  - tss2-sys soname from 1:0:0 to 1:1:0
  - FAPI ignores vendor properties on Fapi_GetInfo
  - FAPI Event Logging JSON format
  - Dead struct TPMS_ALGORITHM_DESCRIPTION
  - Dead field intelPttProperty from TPMU_CAPABLITIES
  - Dead code Tss2_MU_TPMS_ALGORITHM_DESCRIPTION_Marshal
  - Dead code Tss2_MU_TPMS_ALGORITHM_DESCRIPTION_Unmarshal

* Tue Jul 18 2023 jinlun <jinlun@huawei.com> - 3.2.2-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 3.2.2

* Tue Mar 21 2023 jinlun <jinlun@huawei.com> - 3.2.1-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add check code in tpm2-tss

* Tue Jan 31 2023 huangzq6 <huangzhenqiang2@huawei.com> - 3.2.1-2
- Type:CVE
- ID:NA
- SUG:NA
- DESC:fix CVE-2023-22745

* Fri Dec 23 2022 jinlun <jinlun@huawei.com> - 3.2.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 3.2.1

* Sat Jan 29 2022 panxiaohe <panxh.life@foxmail.com> - 3.1.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 3.1.0

* Mon Jan 25 2021 panxiaohe <panxiaohe@huawe.com> - 3.0.3-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 3.0.3
       use proper rundir tmpfilesdir and sysusersdir, so proper directories are used

* Fri Jan 15 2021 yangzhuangzhuang <yangzhuangzhuang@huawei.com> - 2.4.1-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:enable the fapi module

* Thu Jan 14 2021 Hugel<gengqihu1@huawei.com> - 2.4.1-2
- Type:CVE
- ID:NA
- SUG:NA
- DESC: fix CVE-2020-24455

* Wed Jul 29 2020 yang_zhuang_zhuang <yangzhuangzhuang1@huawei.com> - 2.4.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: update version to 2.4.1

* Tue Sep 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.1-4 
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: revise spec file with new rules

* Sat Aug 31 2019 lizaiwang <lizaiwang1@huawei.com> - 2.0.1-3
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: strengthen spec

* Thu Aug 15 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.0.1-2
- Package init
